package com.morpheusdata.cloud

import com.morpheusdata.core.Plugin

class DigitalOceanPlugin extends Plugin {

	@Override
	String getCode() {
		return 'proxmox'
	}

	@Override
	void initialize() {
		//this.name = 'Digital Ocean Plugin'
		ProxmoxCloudProvider cloudProvider = new ProxmoxCloudProvider(this, morpheus)
		ProxmoxProvisionProvider provisionProvider = new ProxmoxProvisionProvider(this, morpheus)
		DigitalOceanBackupProvider backupProvider = new DigitalOceanBackupProvider(this, morpheus)
		ProxmoxOptionSourceProvider optionSourceProvider = new ProxmoxOptionSourceProvider(this, morpheus)
		pluginProviders.put(provisionProvider.code, provisionProvider)
		pluginProviders.put(cloudProvider.code, cloudProvider)
		pluginProviders.put(backupProvider.code, backupProvider)
		pluginProviders.put(optionSourceProvider.code, optionSourceProvider)
		this.setName("ProxMox")
	}

	@Override
	void onDestroy() {

	}
}
